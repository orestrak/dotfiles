# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions


#####################################################################################
### The --clear option make sure Intruder cannot use your existing SSH-Agents keys 
### i.e. Only allow cron jobs to use password less login 
#####################################################################################
/usr/bin/keychain --clear $HOME/.ssh/id_rsa
source $HOME/.keychain/$HOSTNAME-sh

eval `keychain --eval --agents ssh orgmail`


################################
# My aliases
###############################
alias vi='/usr/bin/vim'
alias vim='/usr/bin/vim' 





#############



if [ "$TERM" != "xterm-256color" ]; then
      export TERM=xterm-256color
fi
